package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func main() {

	// method to read file and print contents to terminal
	// f, err := os.Open(os.Args[1])
	// if err != nil {
	// 	fmt.Println("Error:", err)
	// 	os.Exit(1)
	// }
	// io.Copy(os.Stdout, f)

	// alternate method
	dat, err := ioutil.ReadFile(os.Args[1])
	if err != nil {
		fmt.Println("ReadFile Error:", err)
		os.Exit(1)
	}
	fmt.Println(string(dat))
}
